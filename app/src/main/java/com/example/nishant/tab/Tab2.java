package com.example.nishant.tab;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Nishant on 12-Dec-17.
 */

public class Tab2 extends Fragment {
    RecyclerView recyclerView;  //Declaration
    Tab2Adapter adapter; // Create Class MainActivityAdapter to handle Recycle View
    String userdata;
    Context context;
    List<Complist> input = new ArrayList<>();

    public Tab2(){}  //Default constructor


    public Void Get_complaint(String data){
        this.userdata = data;

        return null;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tab2,null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = (RecyclerView)view.findViewById(R.id.recycleView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(linearLayoutManager);
        input.add(new Complist("Data 0"));
        input.add(new Complist("Data 1"));
        input.add(new Complist("Data 2"));
        input.add(new Complist("Data 3"));
        adapter = new Tab2Adapter(input);
        recyclerView.setAdapter(adapter);  //  Extends class RecyclerView.Adapter in MainActivityAdapter class and call override methods in MainActivity adapter such  onCreateViewHolder onBindViewHolder getItemCount
    }

}



