package com.example.nishant.tab;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

/**
 * Created by Nishant on 12-Dec-17.
 */

class Tab2Adapter extends RecyclerView.Adapter<Tab2Adapter.HolderView> {
    private List<Complist> list = new ArrayList<>();  //Create list from Item class

    public Tab2Adapter(List<Complist> input) {
        this.list = input;
    }

    @Override
    public HolderView onCreateViewHolder(ViewGroup parent, int viewType) {

        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.tab2_cardview,null);
        return new HolderView(layout);
    }

    @Override
    public void onBindViewHolder(HolderView holder, int position) {
        holder.textView.setText(list.get(position).getReport());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class HolderView extends RecyclerView.ViewHolder {
        TextView textView;
        public HolderView(View itemView) {
            super(itemView);
            textView = (TextView)itemView.findViewById(R.id.textviewC);
        }
    }
}
