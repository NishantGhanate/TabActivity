package com.example.nishant.tab;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;

/**
 * Created by Nishant on 12-Dec-17.
 */

class FragmentAdapter extends FragmentPagerAdapter {
    public FragmentAdapter(FragmentManager supportFragmentManager) {
        super(supportFragmentManager);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position){
            case 0:{
               Tab1 tab1 = new Tab1();
               return tab1;
            }
            case 1:{
                fragment = new Tab2();
                return fragment;
            }

        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        switch (position){

            case 0:{return "Complaint";}

            case 1:{return  "Status";}
        }
        return super.getPageTitle(position);
    }
}
