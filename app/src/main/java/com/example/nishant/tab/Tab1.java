package com.example.nishant.tab;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by Nishant on 12-Dec-17.
 */

public class Tab1 extends Fragment {

    Button buttonReport;
    EditText editTextComplaint;
    String input;
    Tab2 tab2;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tab1,null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        buttonReport = (Button)view.findViewById(R.id.buttonReport);
        editTextComplaint = (EditText)view.findViewById(R.id.editText);
        buttonReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               input = editTextComplaint.getText().toString();
               editTextComplaint.setText("");
               input=null;
            }
        });
    }
}
